sap.ui.define([
    "sap/ui/redminesapui5/controller/BaseController",
    "sap/ui/model/FilterOperator"
], function (BaseController, FilterOperator) {
    "use strict";
	return BaseController.extend("sap.ui.redminesapui5.controller.Home", {
		
		onLogin:function(oEvent){
			var oView = this.getView();
			var name = oView.byId("employeeId").getValue();
			var city = oView.byId("city").getValue();
            var that=this;
            var uri = "https://cors-anywhere.herokuapp.com/http://services.odata.org/V2/(S(ldbwkn1hbcsh3bief55mfg43))/OData/OData.svc"; // local proxy for cross-domain access

	         // create OData model from URL
	         var oModel = new sap.ui.model.odata.ODataModel(uri, true);
            oModel.oHeader={
            	"MaxDataServiceVersion":"3.0",
				"DataServiceVersion":"3.0"
			};

            oModel.read("/Suppliers", null, null, false, fSuccess, fError);
			function fSuccess(data){
				console.log("Supplier read successfully!")
				var oSuppliers = new sap.ui.model.json.JSONModel(data.results);
				for (var i = 0; i < oSuppliers.oData.length; i++) {
					console.log(oSuppliers.getProperty("/"+i+"/Name"));
					console.log(oSuppliers.getProperty("/"+i+"/Address/City"));
					if (oSuppliers.getProperty("/"+i+"/Name") == name && oSuppliers.getProperty("/"+i+"/Address/City") == city) {
						console.log("Navi to Dashboard page");
                        var oRouter = sap.ui.core.UIComponent.getRouterFor(that);
                        oRouter.navTo("dashboard");
                        break;
					}
				}

			};
			function fError(oEvent){
				console.log("An error occured while reading Employees!")
			};

            // oModel.read("/Customers",null,null,null,function(){
            //     //var metadata = oModel.getServiceMetadata();
            //     var entityCustomerRef = metadata.dataServices.schema[0].entityType[2];
            //     var listOfProperties = entityCustomerRef.property;
            //     console.log(listOfProperties);
            // });
//			var url ="http://rm.devap.jp";
//			var data1 = {"key":"2bb1cc97c99dab3abec47f21b07c14147f99ae39"};
//			function logResults(json,status){
//			  console.log(json);
//			};
			
			//authenticity_token:qxy57I/LpS8LgD649ooFHHQweDZFI0nLY2MGOXrwNg0=
			//05729ab3d0eb1590151d9b508bb91176a702963f
//			$.post( url+"/issues.json",data1, function (data,status,xhr) {
//				console.log(data);
//			},"jsonp");
//			$.ajax({
//				url: url+"/issues.json",
//				dataType: "jsonp",
//				crossDomain: true,
//				jsonpCallback: "logResults",
//				type:"POST",
//				data:data1,
//				error: function(xhr,status,error) {
//					console.log(xhr);
//					alert("fail:"+status);
//				},
//				success: function (data) {
//					console.log(data);
//				},
//			});
		},
		/**
		* Called when a controller is instantiated and its View controls (if available) are already created.
		* Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		* @memberOf redminesapui5.Main
		*/
		onInit: function() {
			console.log("Init Home");
		},
		
		/**
		* Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
		* (NOT before the first rendering! onInit() is used for that one!).
		* @memberOf redminesapui5.Main
		*/
		//	onBeforeRendering: function() {
		//
		//	},
		
		/**
		* Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
		* This hook is the same one that SAPUI5 controls get after being rendered.
		* @memberOf redminesapui5.Main
		*/
		//	onAfterRendering: function() {
		//
		//	},
		
		/**
		* Called when the Controller is destroyed. Use this one to free resources and finalize activities.
		* @memberOf redminesapui5.Main
		*/
		//	onExit: function() {
		//
		//	}
		
		});
});
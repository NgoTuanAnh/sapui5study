sap.ui.define([
    "sap/ui/redminesapui5/controller/BaseController",
    'sap/m/MessageToast',
    'sap/ui/core/Fragment'
], function (BaseController) {
    "use strict";
    return BaseController.extend("sap.ui.redminesapui5.controller.Dashboard", {

        handlePressOpenMenu: function(oEvent) {
            var oButton = oEvent.getSource();

            // create menu only once
            if (!this._menu) {
                this._menu = sap.ui.xmlfragment(
                    "sap.ui.redminesapui5.view.MenuEventing",
                    this
                );
                this.getView().addDependent(this._menu);
            }

            var eDock = sap.ui.core.Popup.Dock;
            this._menu.open(this._bKeyboard, oButton, eDock.BeginTop, eDock.BeginBottom, oButton);
        },



        /**
        * Called when a controller is instantiated and its View controls (if available) are already created.
        * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
        * @memberOf WebContent.webapp.Dashboard **/
        onInit: function() {

        },

        /**
        * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
        * (NOT before the first rendering! onInit() is used for that one!).
        * @memberOf WebContent.webapp.Dashboard
        **/
        onBeforeRendering: function() {

        },

        /**
        * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
        * This hook is the same one that SAPUI5 controls get after being rendered.
        * @memberOf WebContent.webapp.Dashboard **/
        onAfterRendering: function() {

        },

        /**
        * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
        * @memberOf WebContent.webapp.Dashboard
        **/
        onExit: function() {

        }
    });
});
